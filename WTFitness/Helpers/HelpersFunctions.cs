﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WTFitness.Helpers
{
    public static class HelpersFunctions
    {
        public static string ReplacedName(string Name)
        {
            Name = Name.Trim().Replace(" - ", "-").Replace(" ", "-").Replace("/", "-").Replace(@"\", "-").Replace("&", "-").Replace("*", "").Replace("(", "").Replace(")", "").Replace(".", "").Replace("_", "").ToLower();
            return Name;
        }
        public static string ToTitleCase(string Name)
        {
            Name = Name.Replace("_", " ").Replace("-", " ");
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            Name = textInfo.ToTitleCase(Name);
            return Name;
        }
        public static Image FixedSize(Image imgPhoto, int Width, int Height)
        {
            var destRect = new Rectangle(0, 0, Width, Height);
            var destImage = new Bitmap(Width, Height);
            var blankImage = new Bitmap(Width, Height);


            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                using (var wrapMode = new ImageAttributes())
                {

                    graphics.DrawImage(imgPhoto, destRect, 0, 0, imgPhoto.Width, imgPhoto.Height, GraphicsUnit.Pixel, wrapMode);
                }

                graphics.Dispose();
            }

            return destImage;
        }
    }
}
