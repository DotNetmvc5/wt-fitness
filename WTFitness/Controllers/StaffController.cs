﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using WTFitness.Helpers;

namespace WTFitness.Controllers
{
    [Authorize]
    public class StaffController : Controller
    {
        private IStaffRepository userRepository;
        private IUserRepository userRepo;
        private IWebHostEnvironment hostingEnv;
        public StaffController(IStaffRepository _userRepository, IUserRepository _userRepo, IWebHostEnvironment env)
        {
            userRepository = _userRepository;
            userRepo = _userRepo;
            hostingEnv = env;
        }
        public IActionResult Dashboard()
        {
            List<User> user = new List<User>();
            user = userRepo.GetAllUsers();
            return View(user);
        }
        public IActionResult Index()
        {
            List<User> user = new List<User>();
            user = userRepository.GetAllUsers();
            return View(user);
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(User user, IFormCollection form)
        {
            //user.RegistrationDate = DateTime.Now;
            user.IsActive = true;
            //
            user.UserTypeId = 2;
            //
            var fileName = "";
            if (form.Files.Count > 0)
            {
                if (form.Files[0].Length > 0)
                {
                    fileName = HelpersFunctions.ReplacedName(user.Name) + "-" + user.BranchId + "-" + DateTime.Parse(user.RegistrationDate.ToString()).ToString("dd-MM-yyyy") + ".jpg";
                    var webRoot = hostingEnv.WebRootPath;
                    var PathWithFolderName = Path.Combine(webRoot, "images", "users");
                    if (!Directory.Exists(PathWithFolderName))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(PathWithFolderName);
                    }
                    string path = Path.Combine(Directory.GetCurrentDirectory(), PathWithFolderName, fileName);

                    using (var memoryStream = new MemoryStream())
                    {
                        form.Files[0].CopyToAsync(memoryStream);
                        using (var img = Image.FromStream(memoryStream))
                        {
                            HelpersFunctions.FixedSize(img, 320, 320).Save(path, ImageFormat.Jpeg);
                        }
                    }
                }
            }
            user.ImagePath = fileName;
            
            string message = userRepo.CreateUser(user);
            if (!string.IsNullOrEmpty(message))
            {
                TempData["Message"] = message;
                return RedirectToAction();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Edit(int userId)
        {
            User user = new User();
            user = userRepository.GetUserByUserId(userId);

            return View(user);
        }
        [HttpPost]
        public IActionResult Edit(User user, IFormCollection form)
        {
            var fileName = "";
            if (form.Files.Count > 0)
            {
                if (form.Files[0].Length > 0)
                {
                    fileName = HelpersFunctions.ReplacedName(user.Name) + "-" + user.BranchId + "-" + DateTime.Parse(user.RegistrationDate.ToString()).ToString("dd-MM-yyyy") + ".jpg";
                    var webRoot = hostingEnv.WebRootPath;
                    var PathWithFolderName = Path.Combine(webRoot, "images", "users");
                    if (!Directory.Exists(PathWithFolderName))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(PathWithFolderName);
                    }
                    string path = Path.Combine(Directory.GetCurrentDirectory(), PathWithFolderName, fileName);

                    using (var memoryStream = new MemoryStream())
                    {
                        form.Files[0].CopyToAsync(memoryStream);
                        using (var img = Image.FromStream(memoryStream))
                        {
                            HelpersFunctions.FixedSize(img, 320, 320).Save(path, ImageFormat.Jpeg);
                        }
                    }
                }
            }
            user.ImagePath = fileName;
            string message = userRepo.EditUser(user);
            if (!string.IsNullOrEmpty(message))
            {
                TempData["Message"] = message;
                return RedirectToAction();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Delete(int userId)
        {
            User user = new User();
            user = userRepository.GetUserByUserId(userId);
            return View(user);
        }
        [HttpPost]
        public IActionResult Delete(User user)
        {
            userRepository.DeleteUser(user.UserId);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Details(int userId)
        {
            User user = new User();
            user = userRepository.GetUserByUserId(userId);
            return View(user);
        }
        [HttpGet]
        public IActionResult ActiveUsers()
        {
            List<User> user = new List<User>();
            user = userRepository.GetActiveUsers();
            return View("~/Views/Staff/Index.cshtml", user);
        }
        [HttpPost]
        [Route("Staff/ActivateUser")]
        public IActionResult ActivateUser(int userId, bool IsActive)
        {
            User user = new User();
            userRepository.ActivateUser(userId, IsActive);
            return Json(user);
        }
    }
}
