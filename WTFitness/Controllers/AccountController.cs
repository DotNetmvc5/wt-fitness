﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;

namespace WTFitness.Controllers
{
    public class AccountController : Controller
    {
        private IStaffRepository userRepository;
        public AccountController(IStaffRepository _userRepository)
        {
            userRepository = _userRepository;
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(Login login)
        {
            User LoggedInUser = new User();
            LoggedInUser = userRepository.Login(login);
            if (LoggedInUser == null)
            {
                ViewBag.InValidLogin = "Invalid login attempt";
                return View(login);
            }

            var claims = new List<Claim>
                {
                    new Claim("UserRole", LoggedInUser.UserType),
                    new Claim("UserName", LoggedInUser.Name),
                    new Claim("BranchId", LoggedInUser.BranchId.ToString())
                };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                new AuthenticationProperties
                {
                    IsPersistent = true
                });

            return RedirectToAction("Dashboard", "Staff");
            //if(LoggedInUser.BranchId == 1)
            //{
            //    return RedirectToAction("GetUsersByBranch", "Users", new { BranchId = 1 });
            //}
            //else if (LoggedInUser.BranchId == 2)
            //{
            //    return RedirectToAction("GetUsersByBranch", "Users", new { BranchId = 2 });
            //}
            //else
            //{
            //    return RedirectToAction("Index", "Users");
            //}
        }
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login");
        }
    }
}
