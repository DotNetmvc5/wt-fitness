﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using WTFitness.Helpers;

namespace WTFitness.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private IUserRepository userRepository;
        private IWebHostEnvironment hostingEnv;
        public UsersController(IUserRepository _userRepository, IWebHostEnvironment env)
        {
            userRepository = _userRepository;
            hostingEnv = env;
        }
        public IActionResult Index()
        {
            ViewBag.Page = "Index";
            List<User> user = new List<User>();
            user = userRepository.GetAllUsers();
            return View(user);
        }
        public IActionResult GetUsersByBranch(int BranchId)
        {
            ViewBag.Page = "Index";
            List<User> user = new List<User>();
            user = userRepository.GetUsersByBranch(BranchId);
            return View(user);
        }
        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.UserTypes = userRepository.GetAllUserTpes();
            ViewBag.Categories = userRepository.GetAllCategories();
            return View();
        }
        [HttpPost]
        public IActionResult Create(User user, IFormCollection form)
        {
            //user.RegistrationDate = DateTime.Now;
            user.IsActive = true;
            var fileName = "";
            if (form.Files.Count > 0)
            {
                if (form.Files[0].Length > 0)
                {
                    fileName = HelpersFunctions.ReplacedName(user.Name) + "-" + user.BranchId + "-" + DateTime.Parse(user.RegistrationDate.ToString()).ToString("dd-MM-yyyy") + ".jpg";
                    var webRoot = hostingEnv.WebRootPath;
                    var PathWithFolderName = Path.Combine(webRoot, "images", "users");
                    if (!Directory.Exists(PathWithFolderName))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(PathWithFolderName);
                    }
                    string path = Path.Combine(Directory.GetCurrentDirectory(), PathWithFolderName, fileName);

                    using (var memoryStream = new MemoryStream())
                    {
                        form.Files[0].CopyTo(memoryStream);
                        using (var img = Image.FromStream(memoryStream))
                        {
                            HelpersFunctions.FixedSize(img, 320, 320).Save(path, ImageFormat.Jpeg);
                        }
                    }
                }
                else
                {
                    fileName = user.ImagePath;
                }
            }
            else
            {
                fileName = user.ImagePath;
            }
            user.ImagePath = fileName;
            string message = userRepository.CreateUser(user);
            if (!string.IsNullOrEmpty(message))
            {
                TempData["Message"] = message;
                return RedirectToAction();
            }
            if (user.BranchId == 1)
            {
                return RedirectToAction("GetUsersByBranch", "Users", new { BranchId = 1 });
            }
            else if (user.BranchId == 2)
            {
                return RedirectToAction("GetUsersByBranch", "Users", new { BranchId = 2 });
            }
            else
            {
                return RedirectToAction("Index", "Users");
            }
        }
        [HttpGet]
        public IActionResult Edit(int userId)
        {
            User user = new User();
            user = userRepository.GetUserByUserId(userId);

            ViewBag.UserTypes = userRepository.GetAllUserTpes();
            ViewBag.Categories = userRepository.GetAllCategories();

            return View(user);
        }
        [HttpPost]
        public IActionResult Edit(User user, IFormCollection form)
        {
            var fileName = "";
            if (form.Files.Count > 0)
            {
                if (form.Files[0].Length > 0)
                {
                    fileName = HelpersFunctions.ReplacedName(user.Name) + "-" + user.BranchId + "-" + DateTime.Parse(user.RegistrationDate.ToString()).ToString("dd-MM-yyyy") + ".jpg";
                    var webRoot = hostingEnv.WebRootPath;
                    var PathWithFolderName = Path.Combine(webRoot, "images", "users");
                    if (!Directory.Exists(PathWithFolderName))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(PathWithFolderName);
                    }
                    string path = Path.Combine(Directory.GetCurrentDirectory(), PathWithFolderName, fileName);

                    using (var memoryStream = new MemoryStream())
                    {
                        form.Files[0].CopyTo(memoryStream);
                        using (var img = Image.FromStream(memoryStream))
                        {
                            HelpersFunctions.FixedSize(img, 320, 320).Save(path, ImageFormat.Jpeg);
                        }
                    }
                }
                else
                {
                    fileName = user.ImagePath;
                }
            }
            else
            {
                fileName = user.ImagePath;
            }
            user.ImagePath = fileName;
            string message = userRepository.EditUser(user);
            if (!string.IsNullOrEmpty(message))
            {
                TempData["Message"] = message;
                return RedirectToAction();
            }
            if (user.BranchId == 1)
            {
                return RedirectToAction("GetUsersByBranch", "Users", new { BranchId = 1 });
            }
            else if (user.BranchId == 2)
            {
                return RedirectToAction("GetUsersByBranch", "Users", new { BranchId = 2 });
            }
            else
            {
                return RedirectToAction("Index", "Users");
            }
        }
        [HttpGet]
        public IActionResult Delete(int userId)
        {
            User user = new User();
            user = userRepository.GetUserByUserId(userId);
            return View(user);
        }
        [HttpPost]
        public IActionResult Delete(User user)
        {
            userRepository.DeleteUser(user.UserId);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Details(int userId)
        {
            User user = new User();
            MonthFeeUser feeUser = new MonthFeeUser();
            user = userRepository.GetUserByUserId(userId);
            feeUser = userRepository.GetFeePaidByMonthAndUser(userId);
            ViewBag.IsFeePaid = feeUser;
            return View(user);
        }
        [HttpGet]
        public IActionResult ActiveUsers()
        {
            List<User> user = new List<User>();
            user = userRepository.GetActiveUsers();
            return View("~/Views/Users/GetUsersByBranch.cshtml", user);
        }
        [HttpGet]
        public IActionResult ActiveUsersByBranch(int BranchId)
        {
            List<User> user = new List<User>();
            user = userRepository.GetActiveUsersByBranch(BranchId);
            return View("~/Views/Users/GetUsersByBranch.cshtml", user);
        }
        [HttpPost]
        [Route("Users/ActivateUser")]
        public IActionResult ActivateUser(int userId, bool IsActive)
        {
            User user = new User();
            userRepository.ActivateUser(userId, IsActive);
            return Json(user);
        }
        [HttpGet]
        public IActionResult Search(int SearchType)
        {
            ViewBag.SearchType = SearchType;
            return View();
        }
        public IActionResult SearchById(int UserId)
        {
            List<User> user = new List<User>();
            user = userRepository.SearchUserById(UserId);
            return View("~/Views/Users/SearchResult.cshtml", user);
        }
        public IActionResult SearchByName(string Name)
        {
            List<User> user = new List<User>();
            user = userRepository.SearchUserByName(Name);
            return View("~/Views/Users/SearchResult.cshtml", user);
        }
        [HttpGet]
        [Route("Users/SearchByName")]
        public IActionResult SearchByNamePartial(string Name)
        {
            List<User> user = new List<User>();
            user = userRepository.SearchUserByName(Name);
            return PartialView("~/Views/Users/SearchResult.cshtml", user);
        }
        [HttpGet]
        public IActionResult MonthlyPaidUsers(int MonthId, int YearId, int BranchId)
        {
            List<MonthFeeUser> user = new List<MonthFeeUser>();
            ViewBag.CurrentMonthPaid = true;
            user = userRepository.GetMonthlyPaidUsers(MonthId, YearId, BranchId);
            return View("~/Views/Users/FeeManagement.cshtml", user);
        }
        [HttpGet]
        public IActionResult MonthlyUnPaidUsers(int MonthId, int YearId, int BranchId)
        {
            List<MonthFeeUser> user = new List<MonthFeeUser>();
            ViewBag.CurrentMonthPaid = false;
            user = userRepository.GetMonthlyUnPaidUsers(MonthId, YearId, BranchId);
            return View("~/Views/Users/FeeManagement.cshtml", user);
        }
        [HttpPost]
        [Route("Users/FeeUpdate")]
        public IActionResult FeeUpdate(MonthFeeUser feeUser)
        {
            User user = new User();
            feeUser.PaymentDate = DateTime.Now;
            feeUser.ModifiedDate = DateTime.Now;
            userRepository.FeeUpdate(feeUser);
            return Json(user);
        }
        [HttpGet]
        public IActionResult AllTimeFeeManagement(int BranchId)
        {
            ViewBag.BranchId = BranchId;
            return View();
        }
        [HttpGet]
        public IActionResult GetFeeInfoByMonthId(int MonthId, int YearId, int BranchId)
        {
            List<MonthFeeUser> user = new List<MonthFeeUser>();
            ViewBag.AllMonth = true;
            user = userRepository.GetFeeInfoByMonthId(MonthId, YearId, BranchId);
            return PartialView("~/Views/Users/FeeManagement.cshtml", user);
        }
        [HttpGet]
        public IActionResult ChangePassword(int UserId)
        {
            User user = new User();
            user = userRepository.GetUserByUserId(UserId);
            ChangePassword password = new ChangePassword
            {
                UserId = user.UserId
            };
            return View(password);
        }
        [HttpPost]
        public IActionResult ChangePassword(ChangePassword password)
        {
            userRepository.UpdatePassword(password.UserId, password.NewPassword);
            return RedirectToAction("Index","Staff");
        }
    }
}
