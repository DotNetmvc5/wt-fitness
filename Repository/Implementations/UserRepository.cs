﻿using Dapper;
using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Repository.Implementations
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(IConnectionFactory dbConnectionFactory) : base(dbConnectionFactory)
        {

        }
        public List<User> GetAllUsers()
        {
            List<User> user = new List<User>();
            try
            {
                const string storedProcedureName = "GetAllUsers";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserTypeId", 3);
                user = base.DbConnection.Query<User>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public List<User> GetUsersByBranch(int BranchId)
        {
            List<User> user = new List<User>();
            try
            {
                const string storedProcedureName = "GetUsersByBranch";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserTypeId", 3);
                parameters.Add("@BranchId", BranchId);
                user = base.DbConnection.Query<User>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public List<User> GetActiveUsers()
        {
            List<User> user = new List<User>();
            try
            {
                const string storedProcedureName = "GetActiveUsers";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserTypeId", 3);
                user = base.DbConnection.Query<User>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public List<MonthFeeUser> GetMonthlyPaidUsers(int MonthId, int YearId, int BranchId)
        {
            List<MonthFeeUser> user = new List<MonthFeeUser>();
            try
            {
                const string storedProcedureName = "GetMonthlyPaidUsers";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@MonthId", MonthId);
                parameters.Add("@YearId", YearId);
                parameters.Add("@BranchId", BranchId);
                user = base.DbConnection.Query<MonthFeeUser>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public List<MonthFeeUser> GetMonthlyUnPaidUsers(int MonthId, int YearId, int BranchId)
        {
            List<MonthFeeUser> user = new List<MonthFeeUser>();
            try
            {
                const string storedProcedureName = "GetMonthlyUnPaidUsers";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@MonthId", MonthId);
                parameters.Add("@YearId", YearId);
                parameters.Add("@BranchId", BranchId);
                user = base.DbConnection.Query<MonthFeeUser>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public List<User> GetActiveUsersByBranch(int BranchId)
        {
            List<User> user = new List<User>();
            try
            {
                const string storedProcedureName = "GetActiveUsersByBranch";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserTypeId", 3);
                parameters.Add("@BranchId", BranchId);
                user = base.DbConnection.Query<User>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public User GetUserByUserId(int UserId)
        {
            User user = new User();
            try
            {
                const string storedProcedureName = "GetUserByUserId";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserId", UserId);
                user = base.DbConnection.QueryFirstOrDefault<User>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public List<UserTypes> GetAllUserTpes()
        {
            List<UserTypes> user = new List<UserTypes>();
            try
            {
                const string storedProcedureName = "GetAllUserTypes";
                user = base.DbConnection.Query<UserTypes>(storedProcedureName, null, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public List<Category> GetAllCategories()
        {
            List<Category> user = new List<Category>();
            try
            {
                const string storedProcedureName = "GetAllCategories";
                user = base.DbConnection.Query<Category>(storedProcedureName, null, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public string CreateUser(User user)
        {
            string Message = "";
            try
            {
                DateTime DOB;
                bool IsValidDate = DateTime.TryParse(user.DOB.ToString(), out DOB);
                if (!IsValidDate)
                {
                    DOB = DateTime.Now;
                }

                const string storedProcedureName = "CreateUser";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@SerialNumber", user.SerialNumber);
                parameters.Add("@Name", user.Name);
                parameters.Add("@GuardianName", user.GuardianName);
                parameters.Add("@NICNumber", user.NICNumber);
                parameters.Add("@Address", user.Address);
                parameters.Add("@DOB", DOB.Date);
                parameters.Add("@MobileNumber", user.MobileNumber);
                parameters.Add("@EmergencyNumber", user.EmergencyNumber);
                parameters.Add("@BloodGroup", user.BloodGroup);
                parameters.Add("@Email", user.Email);
                parameters.Add("@Occupation", user.Occupation);
                parameters.Add("@RegistrationFee", user.RegistrationFee);
                parameters.Add("@MonthlyFee", user.MonthlyFee);
                parameters.Add("@RegistrationDate", user.RegistrationDate);
                parameters.Add("@IsActive", true);
                parameters.Add("@UserTypeId", user.UserTypeId);
                parameters.Add("@CategoryId", user.CategoryId);
                parameters.Add("@ImagePath", user.ImagePath);
                parameters.Add("@BranchId", user.BranchId);
                parameters.Add("@Password", user.Password);
                parameters.Add("@CardNumber", user.CardNumber);
                parameters.Add("@Remarks", user.Remarks);
                Message = base.DbConnection.QueryFirstOrDefault<string>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return Message;
        }
        public string EditUser(User user)
        {
            string Message = "";
            try
            {
                DateTime DOB;
                bool IsValidDate = DateTime.TryParse(user.DOB.ToString(), out DOB);
                if (!IsValidDate)
                {
                    DOB = DateTime.Now;
                }

                const string storedProcedureName = "EditUser";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Id", user.UserId);
                parameters.Add("@SerialNumber", user.SerialNumber);
                parameters.Add("@Name", user.Name);
                parameters.Add("@GuardianName", user.GuardianName);
                parameters.Add("@NICNumber", user.NICNumber);
                parameters.Add("@Address", user.Address);
                parameters.Add("@DOB", DOB.Date);
                parameters.Add("@MobileNumber", user.MobileNumber);
                parameters.Add("@EmergencyNumber", user.EmergencyNumber);
                parameters.Add("@BloodGroup", user.BloodGroup);
                parameters.Add("@Email", user.Email);
                parameters.Add("@Occupation", user.Occupation);
                parameters.Add("@RegistrationFee", user.RegistrationFee);
                parameters.Add("@MonthlyFee", user.MonthlyFee);
                parameters.Add("@RegistrationDate", user.RegistrationDate);
                parameters.Add("@UserTypeId", user.UserTypeId);
                parameters.Add("@CategoryId", user.CategoryId);
                parameters.Add("@ImagePath", user.ImagePath);
                parameters.Add("@BranchId", user.BranchId);
                parameters.Add("@Password", user.Password);
                parameters.Add("@CardNumber", user.CardNumber);
                parameters.Add("@Remarks", user.Remarks);
                Message = base.DbConnection.QueryFirstOrDefault<string>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return Message;
        }
        public void DeleteUser(int UserId)
        {
            try
            {
                const string storedProcedureName = "DeleteUser";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Id", UserId);
                base.DbConnection.Execute(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
        }
        public void ActivateUser(int userId, bool IsActive)
        {
            try
            {
                const string storedProcedureName = "ActivateUser";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Id", userId);
                parameters.Add("@IsActive", IsActive);
                base.DbConnection.Execute(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
        }
        public List<User> SearchUserById(int UserId)
        {
            List<User> user = new List<User>();
            try
            {
                const string storedProcedureName = "SearchUserById";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserTypeId", 3);
                parameters.Add("@UserId", UserId);
                user = base.DbConnection.Query<User>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public List<User> SearchUserByName(string Name)
        {
            List<User> user = new List<User>();
            try
            {
                const string storedProcedureName = "SearchUserByName";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserTypeId", 3);
                parameters.Add("@UserName", Name);
                user = base.DbConnection.Query<User>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public void FeeUpdate(MonthFeeUser feeUser)
        {
            try
            {
                const string storedProcedureName = "StagingFeeUpdate";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@MonthId", feeUser.MonthId);
                parameters.Add("@UserId", feeUser.UserId);
                parameters.Add("@FeePaid", feeUser.MonthlyFee);
                parameters.Add("@IsPaid", feeUser.IsPaid);
                parameters.Add("@PaymentDate", feeUser.PaymentDate);
                parameters.Add("@ModifiedDate", feeUser.ModifiedDate);
                base.DbConnection.Execute(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
        }
        public MonthFeeUser GetFeePaidByMonthAndUser(int UserId)
        {
            MonthFeeUser feeUser = new MonthFeeUser();
            try
            {
                const string storedProcedureName = "GetFeePaidByMonthAndUser";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserId", UserId);
                parameters.Add("@Month", DateTime.Now.Month);
                parameters.Add("@Year", DateTime.Now.Year);
                feeUser = base.DbConnection.QueryFirstOrDefault<MonthFeeUser>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return feeUser;
        }
        public List<MonthFeeUser> GetFeeInfoByMonthId(int MonthId, int YearId, int BranchId)
        {
            List<MonthFeeUser> user = new List<MonthFeeUser>();
            try
            {
                const string storedProcedureName = "StagingGetFeeInfoByMonthId";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@MonthId", MonthId);
                parameters.Add("@YearId", YearId);
                parameters.Add("@BranchId", BranchId);
                user = base.DbConnection.Query<MonthFeeUser>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public void UpdatePassword(int userId, string NewPassword)
        {
            try
            {
                const string storedProcedureName = "UpdatePassword";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserId" , userId);
                parameters.Add("@NewPassword", NewPassword);
                base.DbConnection.Execute(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
