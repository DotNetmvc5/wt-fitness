﻿using Repository.Enums;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Repository.Implementations
{
    public abstract class BaseRepository
    {
        public IDbConnection DbConnection { get; private set; }
        public BaseRepository(IConnectionFactory dbConnectionFactory)
        {
            this.DbConnection = dbConnectionFactory.CreateDbConnection(DatabaseConnectionName.WTFitness);
        }
    }
}
