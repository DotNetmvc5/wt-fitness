﻿using Dapper;
using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Repository.Implementations
{
    public class StaffRepository : BaseRepository, IStaffRepository
    {
        public StaffRepository(IConnectionFactory dbConnectionFactory) : base(dbConnectionFactory)
        {

        }
        public List<User> GetAllUsers()
        {
            List<User> user = new List<User>();
            try
            {
                const string storedProcedureName = "GetAllUsers";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserTypeId", 2);
                user = base.DbConnection.Query<User>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public List<User> GetActiveUsers()
        {
            List<User> user = new List<User>();
            try
            {
                const string storedProcedureName = "GetActiveUsers";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserTypeId", 2);
                user = base.DbConnection.Query<User>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public User GetUserByUserId(int UserId)
        {
            User user = new User();
            try
            {
                const string storedProcedureName = "GetUserByUserId";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserId", UserId);
                user = base.DbConnection.QueryFirstOrDefault<User>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public List<UserTypes> GetAllUserTpes()
        {
            List<UserTypes> user = new List<UserTypes>();
            try
            {
                const string storedProcedureName = "GetAllUserTypes";
                user = base.DbConnection.Query<UserTypes>(storedProcedureName, null, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public List<Category> GetAllCategories()
        {
            List<Category> user = new List<Category>();
            try
            {
                const string storedProcedureName = "GetAllCategories";
                user = base.DbConnection.Query<Category>(storedProcedureName, null, commandType: CommandType.StoredProcedure).AsList();
            }
            catch (Exception ex)
            {

            }
            return user;
        }
        public void EditUser(User user)
        {
            try
            {
                const string storedProcedureName = "EditUser";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Id", user.UserId);
                parameters.Add("@Name", user.Name);
                parameters.Add("@GuardianName", user.GuardianName);
                parameters.Add("@NICNumber", user.NICNumber);
                parameters.Add("@Address", user.Address);
                parameters.Add("@DOB", DateTime.Parse(user.DOB.ToString()).Date);
                parameters.Add("@MobileNumber", user.MobileNumber);
                parameters.Add("@EmergencyNumber", user.EmergencyNumber);
                parameters.Add("@BloodGroup", user.BloodGroup);
                parameters.Add("@Email", user.Email);
                parameters.Add("@Occupation", user.Occupation);
                parameters.Add("@RegistrationFee", user.RegistrationFee);
                parameters.Add("@MonthlyFee", user.MonthlyFee);
                parameters.Add("@UserTypeId", user.UserTypeId);
                parameters.Add("@CategoryId", user.CategoryId);
                parameters.Add("@ImagePath", user.ImagePath);
                base.DbConnection.Execute(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
        }
        public void DeleteUser(int UserId)
        {
            try
            {
                const string storedProcedureName = "DeleteUser";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Id", UserId);
                base.DbConnection.Execute(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
        }
        public void ActivateUser(int userId, bool IsActive)
        {
            try
            {
                const string storedProcedureName = "ActivateUser";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Id", userId);
                parameters.Add("@IsActive", IsActive);
                base.DbConnection.Execute(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
        }
        public User Login(Login login)
        {
            User user = new User();
            try
            {
                const string storedProcedureName = "Login";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@MobileNumber", login.MobileNumber);
                parameters.Add("@Password", login.Password);
                parameters.Add("@UserTypeId", login.UserTypeId);
                user = base.DbConnection.QueryFirstOrDefault<User>(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return user;
        }
    }
}
