﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Models
{
    public class MonthFee
    {
        public int Id { get; set; }
        public int MonthId { get; set; }
        public int UserId { get; set; }
        public int FeePaid { get; set; }
        public bool IsPaid { get; set; }
    }
    public class MonthFeeUser
    {
        public int MonthId { get; set; }
        public int UserId { get; set; }
        public int SerialNumber { get; set; }
        public string Name { get; set; }
        public int MonthlyFee { get; set; }
        public string MonthName { get; set; }
        public bool IsPaid { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
