﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Repository.Models
{
    public class UserIndex
    {
        public List<User> users { get; set; }
        public UserCountModel countModel { get; set; }
    }
    public class User
    {
        public int UserId { get; set; }
        public int SerialNumber { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }
        [DataType(DataType.Text)]
        public string GuardianName { get; set; }
        public string NICNumber { get; set; }
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string MobileNumber { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [NotMapped]
        [Compare("Password", ErrorMessage = "Password does not match!")]
        public string ConfirmPassword { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string EmergencyNumber { get; set; }
        [DataType(DataType.Text)]
        public string BloodGroup { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.Text)]
        public string Occupation { get; set; }
        [DataType(DataType.Text)]
        public int? RegistrationFee { get; set; }
        [DataType(DataType.Text)]
        public int? MonthlyFee { get; set; }
        [DataType(DataType.Date)]
        public DateTime? RegistrationDate { get; set; }
        public int? CategoryId { get; set; }
        public int? UserTypeId { get; set; }
        public bool IsActive { get; set; }
        public string CategoryName { get; set; }
        public string UserType { get; set; }
        public string ImagePath { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public int? CardNumber { get; set; }
        public string Remarks { get; set; }
    }
    //public class RegisterUser
    //{
    //    [Required]
    //    [DataType(DataType.Text)]
    //    public string Name { get; set; }
    //    [DataType(DataType.Text)]
    //    public string GuardianName { get; set; }
    //    [Required]
    //    public string NICNumber { get; set; }
    //    [Required]
    //    [DataType(DataType.MultilineText)]
    //    public string Address { get; set; }
    //    [Required]
    //    [DataType(DataType.Date)]
    //    public DateTime DOB { get; set; }
    //    [Required]
    //    [DataType(DataType.PhoneNumber)]
    //    public string MobileNumber { get; set; }
    //    [DataType(DataType.PhoneNumber)]
    //    public string EmergencyNumber { get; set; }
    //    [DataType(DataType.Text)]
    //    public string BloodGroup { get; set; }
    //    [DataType(DataType.EmailAddress)]
    //    public string Email { get; set; }
    //    [DataType(DataType.Text)]
    //    public string Occupation { get; set; }
    //    [DataType(DataType.Text)]
    //    public int RegistrationFee { get; set; }
    //    [DataType(DataType.Text)]
    //    public int MonthlyFee { get; set; }
    //    public DateTime RegistrationDate { get; set; }
    //    public int CategoryId { get; set; }
    //    public int UserTypeId { get; set; }
    //}
    //public class UserList
    //{
    //    public int UserId { get; set; }
    //    public string Name { get; set; }
    //    public string MobileNumber { get; set; }
    //    public int MonthlyFee { get; set; }
    //    public DateTime RegistrationDate { get; set; }
    //    public string CategoryName { get; set; }
    //    public string UserType { get; set; }
    //}
    public class UserTypes
    {
        public int UserTypeId { get; set; }
        public string UserType { get; set; }
    }
    public class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
    public class UserCountModel
    {
        public int UserCount { get; set; }
        public int IsActive { get; set; }
    }
    public class ChangePassword
    {
        public int UserId { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        [NotMapped]
        [Compare("NewPassword", ErrorMessage = "Password does not match!")]
        public string ConfirmPassword { get; set; }
    }
}
