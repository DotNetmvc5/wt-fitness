﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interfaces
{
    public interface IStaffRepository
    {
        List<User> GetAllUsers();
        List<User> GetActiveUsers();
        User GetUserByUserId(int UserId);
        List<UserTypes> GetAllUserTpes();
        List<Category> GetAllCategories();
        void EditUser(User user);
        void DeleteUser(int UserId);
        void ActivateUser(int userId, bool IsActive);
        User Login(Login login);
    }
}
