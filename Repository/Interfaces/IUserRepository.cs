﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interfaces
{
    public interface IUserRepository
    {
        List<User> GetAllUsers();
        List<User> GetUsersByBranch(int BranchId);
        List<User> GetActiveUsers();
        List<MonthFeeUser> GetMonthlyPaidUsers(int MonthId, int YearId, int BranchId);
        List<MonthFeeUser> GetMonthlyUnPaidUsers(int MonthId, int YearId, int BranchId);
        List<User> GetActiveUsersByBranch(int BranchId);
        User GetUserByUserId(int UserId);
        List<UserTypes> GetAllUserTpes();
        List<Category> GetAllCategories();
        string CreateUser(User user);
        string EditUser(User user);
        void DeleteUser(int UserId);
        void ActivateUser(int userId, bool IsActive);
        List<User> SearchUserById(int UserId);
        List<User> SearchUserByName(string Name);
        void FeeUpdate(MonthFeeUser feeUser);
        MonthFeeUser GetFeePaidByMonthAndUser(int UserId);
        List<MonthFeeUser> GetFeeInfoByMonthId(int MonthId, int YearId, int BranchId);
        void UpdatePassword(int userId, string NewPassword);
    }
}
